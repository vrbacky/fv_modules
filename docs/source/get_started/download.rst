Download
========

The latest stable release of ProceSeq may be downloaded from
`Bitbucket <https://bitbucket.org/vrbacky/fv_modules/downloads>`__.

Development versions and source code are available on
`Bitbucket <https://bitbucket.org/vrbacky/fv_modules/overview>`__.

