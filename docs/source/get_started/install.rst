Installation
============

Linux
-----

1. Clone ProceSeq from bitbucket

   $ git clone https://bitbucket.org/vrbacky/fv_modules

2. Change directory to fv_modules

   $ cd fv_modules

3. Install fv_modules
   
   $ python3 setup.py install --user


