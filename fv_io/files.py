#!/usr/bin/env python3
"""Optimized functions for file manipulations"""

import sys
import gzip
import os


def open_file(path2file=None, mode="r", encoding="utf-8"):
    """Open file (including error handling).

    Parameters
    ----------
        path2file : str
            Path to a file to be opened (default = None)
        mode : str
            Mode in which the file is opened (default = "r")
        encoding : str
            Encoding in which the file is opened (default = "utf-8")

    Returns
    -------
        file object
            File handle of the opened file
    """

    try:
        if mode.endswith("b") or mode.endswith("b+"):
            return open(path2file, mode=mode)
        else:
            return open(path2file, mode=mode, encoding=encoding)
    except FileNotFoundError:
        sys.exit("File {} not found.".format(path2file))
    except IOError:
        sys.exit("Cannot open file {}.".format(path2file))
    except:
        print("Unexpected error when opening file {}".format(path2file))
        raise


def open_to_read(path2file=None, encoding="utf-8"):
    """Open file for reading

    .gz files are opened using gzip.open().

    Parameters
    ----------
        path2file : str
            Path to a file to be opened (default = None)
        encoding : str
            Encoding in which the file is opened (default = "utf-8")

    Returns
    -------
        file object
            File handle of the opened file
    """

    with open_file(path2file, mode="rb") as filehandle:
        magic = filehandle.read(3)

        if magic.startswith(b'\x1f\x8b\x08'):
            return gzip.open(path2file, mode="rt", encoding=encoding)
        else:
            return open(path2file, mode="r", encoding=encoding)


def open_to_write(path2file=None, mode="w", encoding="utf-8"):
    """Open file for writing.

    Create directory structure, if it doesn't exist.

    Parameters
    ----------
        path2file : str
            Path to a file to be opened (default = None)
        mode : str
            Mode in which the file is opened ("w" or "a") (default = "w")
        encoding : str
            Encoding in which the file is opened (default = "utf-8")

    Returns
    -------
        file object
            File handle of the opened file

    """

    if isinstance(path2file, str):
        directory = os.path.dirname(os.path.abspath(path2file))
        make_directories(directory)
        path = open_file(path2file, mode=mode, encoding=encoding)

    return path


def make_directories(path):
    """Recursively create directory structure.

    Parameters
    ----------
        path : str
            Directory structure to be created

    """

    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except:
            sys.exit("Unable to create foder {}".format(path), file=sys.stderr)
            raise
