VERSION = (0, 1, 0)
__version__ = "0.1.0"

__date__ = "June 27, 2016"
__author__ = "Filip Vrbacky"
__copyright__ = "Copyright 2016 Filip Vrbacky"
__license__ = ("MIT")
