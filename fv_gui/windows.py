#!/usr/bin/env python3

"""Support classes for tkinter based GUI design.

Very simple classes for tkinter based GUI design that can be used
to make root windows and several usefull dialogs.

Example
-------
::

    from tkinter import ttk
    from fv_gui.windows import RootWindow, AboutDialog

    class main():
        def __init__(self, name="", version = "", author = "", years = "",
                     icon_path = None):
            root = RootWindow(name, version, icon_path, 10)

            def clickMe():
                AboutDialog(root.window, name, version, author, years,
                            icon_path)

            action = ttk.Button(root.frm_main, text="About", command=clickMe)
            action.grid(column=0, row=0)

            def close_cmd():
                root.quit()

            btn_close = ttk.Button(root.frm_main, text="Close",
                                   command=close_cmd)
            btn_close.grid(column=1, row=0, padx = (15,0))

            root.window.mainloop()

    main("Foo_bar", "1.0", "Lorem Ipsum", "2016", "./icon.png")

Example code is included in `__name__ == '__main__'` section at the end
of this module and will be executed when this module is used as a standalone
script::

    $ python3 windows.py

"""

import tkinter as tk
from tkinter import ttk, PhotoImage, TclError


class RootWindow():
    """Create root window.

    Parameters
    ----------
    name : str
        Name of the application (default = "").
    version : str
        Version of the application (default = "").
    icon_path : str
        Relative or absolute path to an icon file (default = None).
    padding : int
        Padding of `frm_main` frame in the root window (default: 15).

    Example
    -------
    ::

        import tkinter
        from fv_gui.windows import RootWindow

        root = RootWindow("Foo", "1.0", "./icon.png")
        label = tkinter.Label(root.frm_main, text="Hello world!")
        root.window.mainloop()

    Attributes
    ----------
    window : tkinter.Tk
        Window object
    frm_main : tkinter.ttk.Frame
        Main container for other elements.
    """

    def __init__(self, name="", version="", icon_path=None, padding=15):
        self._name = name
        self._version = version
        self._padding = padding
        self._icon_path = icon_path
        self._create_window()

    def _create_window(self):
        self.window = tk.Tk()
        self.window.title("{0} {1}".format(self._name, self._version))

        if self._icon_path:
            try:
                icon = PhotoImage(file=self._icon_path)
                self.window.tk.call("wm", "iconphoto", self.window._w, icon)
            except TclError:
                print("Failed to open icon file {}.\n".format(self._icon_path))

        self.window.resizable(0, 0)

        self.frm_main = ttk.Frame(self.window)
        self.frm_main.grid(column=0, row=0, padx=self._padding,
                           pady=self._padding)

    def quit(self):
        """Close the root window and exit application."""
        self.window.quit()
        self.window.destroy()
        exit()


class ModalDialog():
    """Create modal dialog.

    Parameters
    ----------
    parent : tkinter.Tk or tkinter.Toplevel
        Parent widget (window or another dialog).
    title : str
        Title of the dialog (default: "").

    Example
    -------
    ::

        from tkinter import ttk
        from fv_gui.windows import ModalDialog, RootWindow

        def open_cmd():
            ModalDialog(root.window, "Modal dialog")

        root = RootWindow("Foo", "1.0", "./icon.png")

        btn_open = ttk.Button(root.window, text="Open", command=open_cmd)
        btn_open.grid(column=0, row=0)

        root.window.mainloop()

    Attributes
    ----------
    window : tkinter.Toplevel
        Modal window
    """

    def __init__(self, parent, title=""):
        self.window = tk.Toplevel(parent)
        self.window.title(title)
        self.window.focus_set()
        self.window.grab_set()
        self.window.transient(parent)
        self.window.resizable(0, 0)

    def quit(self):
        """Close the modal dialog."""
        self.window.destroy()


class AboutDialog(ModalDialog):
    """Create modal *About* dialog

    Parameters
    ----------
    parent : tkinter.Tk or tkinter.Toplevel
        Parent widget (window or another dialog)
    name : str
        Name of the application (default: "")
    version : str
        Version of the application (default: "")
    author : str
        Author of the application (default: "")
    years : str
        Year(s) of publication (default: "")
    img_path : str
        Relative or absolute path to an icon file (default: None)
    license_file_path : str
        Relative or absolute path to a license file (default: None)

    Example
    -------
    ::

        from tkinter import ttk
        from fv_gui.windows import AboutDialog, RootWindow

        name, version, icon_file, license_file_path = "Foo", "1.0",
            "./icon.png", "../LICENSE"

        root = RootWindow(name, version, icon_file)

        def close_cmd():
            AboutDialog(root.window, name,
                        version, "X.Y.", 2016, icon_file, license_file_path)
        btn_close = ttk.Button(root.window, text="Open modal",
                               command=close_cmd)
        btn_close.grid(column=0, row=0)

        root.window.mainloop()

    Attributes
    ----------
    window : tkinter.Toplevel
        Modal *About* window
    """

    def __init__(self, parent, name="", version="", author="", years="",
                 img_path=None, license_file_path=None):
        self._name = name
        self._version = version
        self._author = author
        self._years = years
        self._img_path = img_path
        self._license_file_path = license_file_path
        super().__init__(parent, "About {0} {1}".format(name, version))
        self._create_window()

    def _create_window(self):

        self.window.columnconfigure(0, weight=1)
        self.window.columnconfigure(1, weight=1)

        frm_main = ttk.Frame(self.window)
        frm_main.grid(column=0, row=0, padx=15, pady=15, )

        try:
            img = PhotoImage(file=self._img_path)
            img_label = ttk.Label(frm_main, image=img)
            img_label.image = img
            img_label.grid(column=0, row=0, columnspan=2)
        except:
            pass

        lbl_name = ttk.Label(
            frm_main, text="{} {}".format(self._name, self._version),
            font=("", 16), anchor=tk.CENTER)
        lbl_name.grid(column=0, row=1, columnspan=2, sticky=tk.EW,
                      pady=(10, 0))

        lbl_name = ttk.Label(
            frm_main, text="(c) {} by {}".format(self._years, self._author),
            font=("", 10), anchor=tk.CENTER)
        lbl_name.grid(column=0, row=2, columnspan=2, sticky=tk.EW,
                      pady=(0, 15))

        def _license_cmd():
            LicenseDialog(self.window, self._license_file_path)

        btn_license = ttk.Button(frm_main, text="License",
                                 command=_license_cmd)
        btn_license.grid(column=0, row=4, sticky=tk.W, padx=(0, 10))

        btn_close = ttk.Button(frm_main, text="Close", command=self.quit)
        btn_close.grid(column=1, row=4, sticky=tk.E)
        btn_close.focus()


class LicenseDialog(ModalDialog):
    """Create modal *License* dialog.

    Parameters
    ----------
    parent : tkinter.Tk or tkinter.Toplevel
        Parent widget (window or another dialog)
    license_file_path : str
        Path to license file (default: "../LICENSE")
    title : str
        Title of the dialog (default: "License")

    Example
    -------
    ::

        from tkinter import ttk
        from fv_gui.windows import AboutDialog, RootWindow

        name, version, icon_file, license_file_path = "Foo", "1.0",
            "./icon.png", "../LICENSE"

        root = RootWindow(name, version, icon_file)

        def close_cmd():
            AboutDialog(root.window, name,
                        version, "X.Y.", 2016, icon_file, license_file_path)
        btn_close = ttk.Button(root.window, text="Open modal",
                               command=close_cmd)
        btn_close.grid(column=0, row=0)

        root.window.mainloop()

    Attributes
    ----------
    window : tkinter.Toplevel
        Modal *License* dialog

    """

    def __init__(self, parent, license_file_path="../LICENSE",
                 title="License"):
        self.license_file_path = license_file_path
        super().__init__(parent, title)
        self._create_window()

    def _create_window(self):

        frm_main = ttk.Frame(self.window)
        frm_main.grid(column=0, row=0, padx=10, pady=10)

        try:
            self.license_text = self._read_text_file(self.license_file_path)
        except FileNotFoundError:
            print("{} file not found. License information left empty.\n".
                  format(self.license_file_path))
            self.license_text = ""
        except TypeError:
            print("Wrong type of license file path information. License "
                  "information left empty.\n")
            self.license_text = ""

        txt_license = tk.Text(frm_main, wrap=tk.WORD, width=70, height=20)
        txt_license.grid(column=0, row=0, sticky=tk.NE)

        scroller = tk.Scrollbar(frm_main, width=12)
        scroller.grid(column=1, row=0, sticky=tk.NS)
        scroller.config(command=txt_license.yview)
        txt_license["yscrollcommand"] = scroller.set

        txt_license.insert(tk.INSERT, self.license_text)

        btn_close = ttk.Button(frm_main, text="Close", command=self.quit)
        btn_close.grid(column=0, columnspan=2, row=1, pady=(10, 0),
                       sticky=tk.E)
        btn_close.focus()

    def _read_text_file(self, file_path):
        return open(file_path, "r").read()


if __name__ == "__main__":  # pragma: no cover

    class main():
        def __init__(self, name="", version="", author="", years="",
                     icon_path=None):
            root = RootWindow(name, version, icon_path, 10)

            def clickMe():
                AboutDialog(root.window, name, version, author, years,
                            icon_path, "../LICENSE")

            action = ttk.Button(root.frm_main, text="About", command=clickMe)
            action.grid(column=0, row=0)

            def close_cmd():
                root.quit()

            btn_close = ttk.Button(root.frm_main, text="Close",
                                   command=close_cmd)
            btn_close.grid(column=1, row=0, padx=(15, 0))

            root.window.mainloop()

    main("Foo_bar", "1.0", "Lorem Ipsum", "2016", "./icon.png")
