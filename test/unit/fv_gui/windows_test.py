import unittest
import tkinter
from fv_gui import windows
from tkinter import TclError, PhotoImage, ttk
import os

class test_Window(unittest.TestCase):
    
    def test_Window_instance_wrong_icon_file_doesnt_raise_an_error(self):
        raised = False
        try:
            windows.RootWindow("", "", "foo.bar")
        except:
            raised = True
        self.assertFalse(raised, "Exception raised")
             
    def test_Window_instance_has_window_of_tkinter_type(self):
        wnd = windows.RootWindow()
        self.assertIsInstance(wnd.window, tkinter.Tk)
        
    def test_Window_instance_has_frame_frm_main(self):
        wnd = windows.RootWindow()
        self.assertIsInstance(wnd.frm_main, tkinter.ttk.Frame)


if __name__ == "__main__":
    unittest.main()