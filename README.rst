==========
fv_modules
==========

Auxiliary modules used for easier I/O, GUI, etc. operations.

Requirements
============

- `Python >= 3.4.0 <http://python.org>`__
- `Biopython >= 1.65 <http://biopython.org>`__

Download
========

The latest stable release of ProceSeq may be downloaded from
`Bitbucket <https://bitbucket.org/vrbacky/fv_modules/downloads>`__.

Development versions and source code are available on
`Bitbucket <https://bitbucket.org/vrbacky/fv_modules/overview>`__.

Installation
============

Linux
-----

1. Clone ProceSeq from bitbucket

   $ git clone https://bitbucket.org/vrbacky/fv_modules

2. Change directory to fv_modules

   $ cd fv_modules

3. Install fv_modules
   
   $ python3 setup.py install --user


Contact
=======

If you have questions you can email
`Filip Vrbacky <mailto:vrbacky@fnhkcz>`__.

If you've discovered a bug or have a feature request, you can create an issue
on Bitbucket using the
`Issue Tracker <https://bitbucket.org/vrbacky/fv_modules/issues>`__.

