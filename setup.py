#!/usr/bin/env python3
"""fv_modules setup"""

import codecs
import os
import re
import sys

try:
    from pip.req import parse_requirements
except ImportError:
    raise RuntimeError('Please install pip before installing fv_modules.\n')
try:
    from setuptools import setup, find_packages
except ImportError:
    raise RuntimeError("Python package setuptools hasn't been installed.\n"
                       "Please install setuptools before installing filda.\n")
if sys.version_info < (3, 4, 0):
    raise RuntimeError("Python 3.4.0 or higher required.\n")

NAME = "fv_modules"


def read(*parts):
    here = os.path.abspath(os.path.dirname(__file__))
    return codecs.open(os.path.join(here, *parts), "r").read()


def get_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


def get_long_description():
    try:
        with open("README.rst", "r") as readme_file:
            return readme_file.read()
    except:
        print("Cannot find README.rst file. Long description will be empty.\n")
        return ""


def get_requirements():
    requirements_file = "requirements.txt"
    try:
        requirements = parse_requirements(requirements_file, session=False)
    except TypeError:
        requirements = parse_requirements(requirements_file)
    return [str(r.req) for r in requirements]


def get_scripts():
    scripts = []
    return [os.path.join('bin', i) for i in scripts]


setup(name=NAME,
      version=get_version(NAME, "__init__.py"),
      description="Auxiliary modules used for easier I/O, GUI, etc. operations.",
      long_description=get_long_description(),
      install_requires=get_requirements(),
      keywords="python auxiliary method",
      url="https://bitbucket.org/vrbacky/fv_modules",
      author="Filip Vrbacky",
      author_email="vrbacky@fnhk.cz",
      maintainer="Filip Vrbacky",
      maintainer_email="vrbacky@fnhk.cz",
      license=("MIT"),
      zip_safe=False,
      packages=find_packages(exclude=["bin", "docs", "test"]),
      scripts=get_scripts(),
      test_suite="test",
      classifiers=["Development Status :: 1 - Planning",
                   "Environment :: Console",
                   "Intended Audience :: Science/Research",
                   "Natural Language :: English",
                   "Operating System :: OS Independent",
                   "Programming Language :: Python :: 3.4",
                   "Programming Language :: Python :: 3.5"
                   "Topic :: Scientific/Engineering :: Bio-Informatics"]
      )
